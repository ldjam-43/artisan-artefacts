-- this displays a dialog asking if the user really wants to quit the game
exitDialogActive = false

exitDialogWidth = 500
exitDialogHeight = 200
exitDialogPosX = (CANVAS_WIDTH/2)-(exitDialogWidth/2)
exitDialogPosY = 300

exitButtonWidth = 200
exitButtonHeight = 70
exitButtonPosX = exitDialogPosX+(exitDialogWidth/2)-(exitButtonWidth+20)
exitButtonPosY = exitDialogPosY+exitDialogHeight-(exitButtonHeight+20)

noexitButtonWidth = exitButtonWidth
noexitButtonHeight = exitButtonHeight
noexitButtonPosX = exitDialogPosX+exitDialogWidth-(noexitButtonWidth+20)
noexitButtonPosY = exitButtonPosY

function showExitDialog()
    exitDialogActive = true 
end

function hideExitDialog()
    exitDialogActive = false
end

function drawExitDialog()
    --print(exitDialogActive)
    if exitDialogActive then
        love.graphics.push() 
   

        -- draw box with outline
        love.graphics.setLineWidth(2)
        love.graphics.setColor(0.8, 0.6, 0.4, 1)
        love.graphics.rectangle("fill", exitDialogPosX, exitDialogPosY, exitDialogWidth, exitDialogHeight, orderBoxCornerRadius)
        love.graphics.setColor(0.1, 0.1, 0.1, 1)
        love.graphics.rectangle("line", exitDialogPosX, exitDialogPosY, exitDialogWidth, exitDialogHeight, orderBoxCornerRadius)
        -- draw text
        local text = "Do you really want to exit?"
        love.graphics.setFont(oldfont)
        love.graphics.printf(text, exitDialogPosX + 20, exitDialogPosY + 20, exitDialogWidth-40)
        -- draw buttons
        local x, y = love.mouse.getPosition()

        --draw rounded rectangle with outline for exit button

        if inBox(x, y, exitButtonPosX, exitButtonPosY, exitButtonWidth, exitButtonHeight) then
            love.graphics.setColor(0.9, 0.7, 0.5, 1)
        else
            love.graphics.setColor(0.7, 0.5, 0.3, 1)
        end
        love.graphics.rectangle("fill", exitButtonPosX, exitButtonPosY, exitButtonWidth, exitButtonHeight, buttonCornerRadius)
        love.graphics.setColor(0.1, 0.1, 0.1, 1)
        love.graphics.rectangle("line", exitButtonPosX, exitButtonPosY, exitButtonWidth, exitButtonHeight, buttonCornerRadius)
        ---draw text
        local text = "Yes"
        love.graphics.printf(text, exitButtonPosX + 10, exitButtonPosY + 10, exitButtonWidth-20, "center")

        --draw rounded rectangle with outline for don't exit button

        if inBox(x, y, noexitButtonPosX, noexitButtonPosY, noexitButtonWidth, noexitButtonHeight) then
            love.graphics.setColor(0.9, 0.7, 0.5, 1)
        else
            love.graphics.setColor(0.7, 0.5, 0.3, 1)
        end
        love.graphics.rectangle("fill", noexitButtonPosX, noexitButtonPosY, noexitButtonWidth, noexitButtonHeight, buttonCornerRadius)
        love.graphics.setColor(0.1, 0.1, 0.1, 1)
        love.graphics.rectangle("line", noexitButtonPosX, noexitButtonPosY, noexitButtonWidth, noexitButtonHeight, buttonCornerRadius)
        ---draw text
        local text = "No"
        love.graphics.printf(text, noexitButtonPosX + 10, noexitButtonPosY + 10, noexitButtonWidth-20, "center")
        love.graphics.setFont(mainfont)

        love.graphics.pop()
    end
end
