require "assets"
local class = require "lib.middleclass"

overlapCountingStep = 10

-- Item ----------------------------------------------------------

Item = class("Item")

function Item:initialize(name)
    self.name = name
    self.image = images[name]
    self.imageData = imageDatas[name]
    assert(self.image, "Image not found for " .. name)
    self.parent = nil
    self.isItem = true 
    self.pixelCount = nil
    self.transform = love.math.newTransform(-self.image:getWidth() / 2, -self.image:getHeight())
end

function Item:draw()
    love.graphics.push()
    love.graphics.applyTransform(self.transform)
    if desaturateItems and self.isMaterialItem then
        effect:send("colorMatrix", desaturationMatrix)
    elseif self.isHighlighted then
        effect:send("colorMatrix", lighterMatrix)
    else
        effect:send("colorMatrix", identityMatrix)
    end
    love.graphics.draw(self.image)
    love.graphics.pop()
end

function Item:resetTransform()
    local w, h = self.image:getWidth(), self.image:getHeight()
    self.transform = love.math.newTransform(-w * baseScale/2, -h * baseScale/2, 0, baseScale, baseScale)
end

function Item:getPixelCount()
    if self.pixelCount == nil then
        self.pixelCount = 0
        local step = 10
        local w, h = self.image:getWidth()-2, self.image:getHeight()-2
        for x = 1, w, step do
            for y = 1, h, step do
                r, g, b, a = self.imageData:getPixel(x, y)
                if a > 0.5 then
                    self.pixelCount = self.pixelCount + 1
                end
            end
        end
    end
    return self.pixelCount
end

function Item:getGlobalTransform()
    if self.parent == nil then
        return self.transform
    else
        return self.parent:getGlobalTransform() * self.transform
    end
end

function Item:getPixelOverlap(other)
    local myTransform = self:getGlobalTransform()
    local otherTransform =  other:getGlobalTransform()
    assert(myTransform)
    assert(otherTransform)
    local crossTransform = otherTransform:inverse() * myTransform
    local overlap = 0
    
    local w, h = self.image:getWidth()-2, self.image:getHeight()-2
    local otherW, otherH = other.image:getWidth()-2, other.image:getHeight()-2
    for x = 1, w, overlapCountingStep do
        for y = 1, h, overlapCountingStep do
            r, g, b, a = self.imageData:getPixel(x, y)
            if a > 0.5 then
                otherX, otherY = crossTransform:transformPoint(x, y)
                if otherX >= 1 and otherX < otherW and otherY >= 1 and otherY < otherH then
                    otherR, otherG, otherB, otherA = other.imageData:getPixel(otherX, otherY)
                    if otherA > 0.5 then
                        --other.imageData:setPixel(otherX, otherY, 1,0,0,1)
                        overlap = overlap + 1
                    end
                end
            end
        end
    end
    --if overlap > 0 then
    --    other.image = love.graphics.newImage(other.imageData)
    --end
    return overlap
end

function Item:getPixelOverlapByMultiple(others)
    local w, h = self.image:getWidth()-2, self.image:getHeight()-2
    local transformCache = {}
    local overlap = 0
    local myTransform = self:getGlobalTransform()
    for i, other in pairs(others) do
        transformCache[other] = other:getGlobalTransform():inverse() * myTransform
    end
    for x = 1, w, overlapCountingStep do
        for y = 1, h, overlapCountingStep do
            r, g, b, a = self.imageData:getPixel(x, y)
            if a > 0.5 then
                overlap = overlap + self:getOverlapForPixel(x, y, others, transformCache)
            end
        end
    end
    return overlap
end

function Item:getOverlapForPixel(x, y, others, transformCache)
    for i, other in pairs(others) do
        local crossTransform = transformCache[other]
        local otherX, otherY = crossTransform:transformPoint(x, y)
        local otherW, otherH = other.image:getWidth()-2, other.image:getHeight()-2
        if otherX >= 1 and otherX < otherW and otherY >= 1 and otherY < otherH then
            otherR, otherG, otherB, otherA = other.imageData:getPixel(otherX, otherY)
            if otherA > 0.5 then
                --other.imageData:setPixel(otherX, otherY, 1,0,0,1)
                return 1
            end
        end
    end
    return 0
end

function Item:centerString()
    local x, y = self:getCenter()
    return "(" .. x .. "," .. y .. ")"
end

function Item:getCenter() -- in global coordinated
    return self:getPoint(self.image:getWidth() / 2, self.image:getHeight() / 2)
end

function Item:getPoint(x, y) -- from texture space to global coordinated
    local myTransform = self:getGlobalTransform()
    local tx, ty = myTransform:transformPoint(x,y)
    return tx, ty
end

function Item:getBoundingBox(otherTransform)
    local myTransform = self:getGlobalTransform()
    if otherTransform then
        myTransform = otherTransform:inverse() * myTransform
        --myTransform:apply(otherTransform)
    end
    local w = self.image:getWidth()
    local h = self.image:getHeight()
    local bb = Rect:new()
   
    bb:includePoint(myTransform:transformPoint(0,0))
    bb:includePoint(myTransform:transformPoint(w,0))
    bb:includePoint(myTransform:transformPoint(0,h))
    bb:includePoint(myTransform:transformPoint(w,h))

    return bb
end

-- ShapeItem ----------------------------------------------------------

ShapeItem = class("ShapeItem", Item)

function ShapeItem:draw()
    love.graphics.push()
    love.graphics.setColor(1, 1, 1, 0.2)
    effect:send("colorMatrix", identityMatrix)
    love.graphics.applyTransform(self.transform)
    love.graphics.draw(self.image)
    love.graphics.pop()
end

-- MaterialItem ----------------------------------------------------------

MaterialItem = class("MaterialItem", Item)

function MaterialItem:initialize(name)
    Item.initialize(self, name)
    assert(itemCatalog[name], "Item catalog entry not found for " .. name)
    self.properties = itemCatalog[name]
    self.isMaterialItem = true
    -- set default value 0 for all properties not defined in item catalog:
    for i, property in pairs(propertyCatalog) do
        if self.properties[property] == nil then
            self.properties[property] = 0
        end
    end
end

-- Helpers ----------------------------------------------------------

function createRandomItem()
    local itemNames = {}
    for name, properties in pairs(itemCatalog) do
        table.insert(itemNames, name)
    end
    local name = itemNames[math.random(#itemNames)]
    return MaterialItem:new(name)
end