require "rectangle"
local class = require "lib.middleclass"

--[[
Jede von ItemGroup abgeleitete Klasse muss diese Methoden implementieren:

    bool canAddItem(item)   - prüft ob das Item mit seinem aktuellen transform hinzugefügt werden kann.
    bool addItem(item)      - fügt das Item mit seinem aktuellen transform hinzu. Muss becomeParent aufrufen.
    array? getItemsAt(x, y) - gibt Items von der Position (gloable Koordinaten) zurück, ohne es zu entfernen.
    bool removeItem(item)   - entfernt ein item.

Wenn items unabhängig von ihrem aktuellen transform hinzugefügt werden sollen, muss zudem auch diese Methode implementiert werden:

    bool canAddItemAnywhere(item) - prüft ob das Item irgendwo hinzugefügt werden kann.
    bool addItemAnywhere(item)    - fügt das Item irgendwo hinzu. Muss becomeParent aufrufen.

ItemGroup bietet zum Hinzufügen und entfernen von mehreren Items auf einmal die folgendenen Abkürzungen

    bool canAddItems(array)  - ruft für jedes item canAddItem auf und gibt nur true zurück, wenn alle gehen.
    bool addItems(array)     - fügt nacheinander jedes item mit addItem hinzu.
    bool canAddItemsAnywhere(array)  - ruft für jedes item canAddItemAnywhere auf und gibt nur true zurück, wenn alle gehen.
    bool addItemsAnywhere(array)     - fügt nacheinander jedes item mit addItemAnywhere hinzu.
    bool removeItems(array)  - entfernt nacheinander jedes item mit removeItem.
    
]]

-- item group
ItemGroup = class("ItemGroup")

function ItemGroup:initialize(rect)
    self.transform = love.math.newTransform(0, 0)
    self.children = {}
    self.isItemGroup = true
    self.rect = rect or Rect:new(0,0,CANVAS_WIDTH,CANVAS_HEIGHT)
end

-- abstract methods - please implement in subclasses

-- Returns true if the given item can be added, taking into account the current transform of the item and other circumstances. You should call this before attempting addItem, and if this returns false, do not call addItem.
function ItemGroup:canAddItem(item)
    assert(false, "Must implement canAddItem in " .. tostring(self))
    -- return self.rect:isRectInside(item:getBoundingBox(self:getGlobalTransform()))
end

-- Adds the item using the current transform. Call canAddItem before and if it returns false, don't call this method. No return value.
function ItemGroup:addItem(item)
    assert(false, "Must implement addItem in " .. tostring(self))
end

-- Returns all the items that can be picked up at this place.
function ItemGroup:getItemsAt(x, y)
    assert(false, "Must implement getItemsAt in " .. tostring(self))
end

function ItemGroup:removeItem(item)
    assert(false, "Must implement removeItem in " .. tostring(self))
end

function ItemGroup:canAddItemAnywhere(item)
    assert(false, "If you want to use this feature, you must implement canAddItemAnywhere in " .. tostring(self))
end

function ItemGroup:addItemAnywhere(item)
    assert(false, "If you want to use this feature, you must implement addItemAnywhere in " .. tostring(self))
end


-- batch helpers
function ItemGroup:canAddItems(items)
    for i, item in pairs(items) do
        if not self:canAddItem(item) then
            return false
        end
    end
    return true
end

function ItemGroup:addItems(items)
    --assert(self:canAddItems(items))
    for i, item in pairs(items) do
        self:addItem(item)
    end
end

function ItemGroup:canAddItemsAnywhere(items)
    for i, item in pairs(items) do
        if not self:canAddItemAnywhere(item) then
            return false
        end
    end
    return true
end

function ItemGroup:addItemsAnywhere(items)
    assert(self:canAddItemsAnywhere(items))
    for i, item in pairs(items) do
        self:addItemAnywhere(item)
    end
end

function ItemGroup:removeItems(items)
    for i, item in pairs(items) do
        self:removeItem(item)
    end
end

-- others
function ItemGroup:becomeParent(item)
    assert(item.parent == nil)
    -- TODO: check the adjusments of transforms. This is important!
    -- item.transform = self:getGlobalTransform():inverse() * item.getGlobalTransform()
    item.parent = self
    table.insert(self.children, item)
end

function ItemGroup:stopBeingParent(item)
    assert(item.parent == self)
    item.parent = nil
    for i = #self.children, 1, -1 do
        child = self.children[i]
        if child == item then
            table.remove(self.children, i)
            return
        end
    end
end

function ItemGroup:childCount()
    return #self.children
end

function ItemGroup:draw()
    love.graphics.push()
    love.graphics.applyTransform(self.transform)
    --self.rect:draw()
    love.graphics.setShader(effect)
    self:drawChildren()
    love.graphics.setShader()
    love.graphics.pop()
end

function ItemGroup:drawChildren() 
    for index,child in pairs(self.children) do
        assert(child.isItem)
        child:draw()
    end
end

function ItemGroup:getGlobalTransform()
    return self.transform
end