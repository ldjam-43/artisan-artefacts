require "scenegraph.itemgroup"
require "scenegraph.spatialitemgroup"
require "scenegraph.item"

local class = require "lib.middleclass"
Cursor = class("Cursor", SpatialItemGroup)

-- constructor
function Cursor:initialize()
   ItemGroup.initialize(self)
   self.x = 0
   self.y = 0
   self.rotation = 0
   self.hasOverlappingChild = false
   self.rect = Rect:new(-50, -50, 50, 50)

   invisibleMouseCursor = love.mouse.newCursor(love.image.newImageData(1, 1), 0, 0)
end

-- overrides
function Cursor:canAddItem(item)
    return true
end

function Cursor:getItemsAt(x, y)
    assert(false, "Don't call getItemsAt on " .. tostring(self))
end

-- others
function Cursor:drawChildren()
    if #self.children > 0 and not exitDialogActive then
        love.mouse.setCursor(invisibleMouseCursor)
    else
        love.mouse.setCursor()
    end

    for index,child in pairs(self.children) do
        assert(child.isItem)
    end
    if self.canPut then
        desaturateItems  = false
    else
        desaturateItems  = true
    end
    ItemGroup.drawChildren(self)
    desaturateItems  = false
    --love.graphics.draw(images.handcursor, -images.handcursor:getWidth() / 2, -images.handcursor:getHeight() / 2)
end

function Cursor:updatePosition(x, y) 
    self.x = x
    self.y = y
    self:moved()
end

function Cursor:rotate(deltaDegrees) 
    self.rotation = self.rotation + math.rad(deltaDegrees)
    self:moved()
end

function Cursor:moved()
    screen:resetHighlights()
    self.transform = love.math.newTransform(self.x, self.y, self.rotation)
    if self:childCount() > 0 then
        self.canPut = screen:canPutItems(self.x, self.y)
    else
        self.canPut = false
        screen:updateCanTake(x, y)
    end
end