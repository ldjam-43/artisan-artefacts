local class = require "lib.middleclass"
require "scenegraph.itemgroup"

Bin = class("Bin", ItemGroup)
function Bin:initialize(rect)
    ItemGroup.initialize(self, rect)
end

function Bin:canAddItem(item)
    return self.rect:intersects(item:getBoundingBox())
end

function Bin:addItem(item)
    assert(self:canAddItem(item))
    if item.parent then
        item.parent:removeItem(item)
    end
    sounds.bin:setPitch(math.random(80, 120)/100)
    sounds.bin:play()
end

function Bin:getItemsAt(x, y)
    return {}
end

function Bin:removeItem(item)
    assert(false, "Never call removeItem on " .. tostring(self))
end
