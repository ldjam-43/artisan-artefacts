local class = require "lib.middleclass"
require "scenegraph.itemgroup"

Clutterbox = class("Clutterbox", ItemGroup)
function Clutterbox:initialize(rect)
    ItemGroup.initialize(self, rect)
end

function Clutterbox:canAddItem(item)
    return false
end

function Clutterbox:addItem(item)
    assert(false, "Never call addItem on " .. tostring(self))
end

function Clutterbox:getItemsAt(x, y)
    if self.rect:isPointInside(x, y) then
        self.currentItem = createRandomItem()
        self.currentItem:resetTransform()
        return { self.currentItem }
    else
        self.currentItem = nil
        return {}
    end
end

function Clutterbox:removeItem(item)
    assert(item == self.currentItem, "Can't remove any other item then the one you got from the last call to getItemsAt.")
    item.transform = screen.cursor.transform * item.transform
    sounds.searching:setPitch(math.random(80, 120)/100)
    sounds.searching:play()
    self.currentItem = nil
end
