local class = require "lib.middleclass"
require "scenegraph.itemgroup"

SpatialItemGroup = class("SpatialItemGroup", ItemGroup)
function SpatialItemGroup:initialize(rect)
    ItemGroup.initialize(self, rect)
end

function SpatialItemGroup:canAddItem(item)
    return self.rect:intersects(item:getBoundingBox())
end

function SpatialItemGroup:addItem(item)
    assert(item.parent == nil)

    --print("spatial item noch lose: " .. item:centerString())
    item.transform = self.transform:inverse() * item.transform
    --print("spatial item re-transformiert: " .. item:centerString())
    item.parent = self
    table.insert(self.children, item)
    --print("spatial item eingefügt: " .. item:centerString())
end

function SpatialItemGroup:getItemsAt(mouseX, mouseY) 
    -- TODO return the full subtree that would be disconnected
    -- iterate in reverse order, so that uppermost image will be found first
    for i = #self.children, 1, -1 do
        child = self.children[i]
        assert(child.isItem)
        childTransform = self.transform * child.transform
        x, y = childTransform:inverseTransformPoint(mouseX, mouseY)
        image = child.image
        if x >= 0 and y >= 0 and x < image:getWidth() and y < image:getHeight() then
            r, g, b, a = child.imageData:getPixel(x,y)
            if a > 0.5 then
                return self:extendSelection(child)
            end
        end
    end

    return { }
end

function SpatialItemGroup:removeItem(item)
    --print("spatial item noch dran: " .. item:centerString())
    item.transform = self.transform * item.transform
    --print("spatial item re-transformiert: " .. item:centerString())
    self:stopBeingParent(item)
    --print("spatial item nun lose: " .. item:centerString())
end