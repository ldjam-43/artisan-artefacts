require "scenegraph.sculpture"
require "scenegraph.cursor"
require "scenegraph.shelf"
require "scenegraph.clutterbox"
require "scenegraph.bin"

local class = require "lib.middleclass"
Screen = class("Screen")

function Screen:initialize()
    self.cursor = Cursor:new()
    self.shelf = Shelf:new(Rect:new(2672/2, 106/2, 3584 / 2, 1493 / 2), 4, 7)
    self.clutterbox = Clutterbox:new(Rect:new(2718/2, 1608/2,3810/2,1902/2))
    --self.bin = Bin:new(Rect:new(140/2, 1545/2, 510/2, 1720/2))
    self.children = {self.shelf, self.clutterbox}--, self.bin}
end

function Screen:setSculpture(sculpture)
    self.sculpture = sculpture
    self.children = {self.sculpture, self.shelf, self.clutterbox}--, self.bin}
    --self.children = {self.sculpture}
end

function Screen:draw() 
    for i, child in pairs(self.children) do
        if child then
            child:draw()
        end
    end
    self.cursor:draw()
end

function Screen:handleClick(x,y)
    local cursorCount = self.cursor:childCount()
    if cursorCount == 0 then
        self:tryGetItems(x,y)
    else
        self:tryPutItems(x,y)
    end
end

function Screen:tryGetItems(x,y)
    for i, group in pairs(self.children) do
        if group then
            potItems = group:getItemsAt(x, y)
            if #potItems > 0 then
                group:removeItems(potItems)
                for n, item in pairs(potItems) do
                    item.isHighlighted = false
                end
                self.cursor:addItems(potItems)
                return
            end
        end
    end
end

function Screen:tryPutItems(x, y)
    local items = {}
    for i, child in pairs(self.cursor.children) do
        items[i] = child
    end
    for i, group in pairs(self.children) do
        if group then
            if group:canAddItems(items) then
                self.cursor:removeItems(items)
                group:addItems(items)
                return
            end
        end
    end
end

function Screen:canPutItems(x, y)
    for i, group in pairs(self.children) do
        if group and group:canAddItems(self.cursor.children) then
            return true
        end
    end
    return false
end

function Screen:resetHighlights()
    for i, group in pairs(self.children) do
        if group then
            for i, item in pairs(group.children) do
                item.isHighlighted = false
            end
        end
    end
end

function Screen:updateCanTake(x, y)
    for i, group in pairs(self.children) do
        if group then
            potItems = group:getItemsAt(x, y)
            if #potItems > 0 then
                for i, item in pairs(potItems) do
                    item.isHighlighted = true
                end
                return
            end
        end
    end
end
