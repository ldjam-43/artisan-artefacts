require "rectangle"
require "scenegraph.itemgroup"
local class = require "lib.middleclass"
Shelf = class("Shelf", ItemGroup)

-- constructor
function Shelf:initialize(rect, rows, cols)
    ItemGroup.initialize(self)
    self.spaces = {}
    self.rect = rect;
    self.rowCount = rows
    self.colCount = cols
    self.shelfSpaceHeightInPx = rect:getHeight() / rows
    self.shelfSpaceWidthInPx = rect:getWidth() /cols
    self.isShelf = true
    self.transform = self.rect:extractTransform()

	for row=1,rows do
        self.spaces[row] = {}
		for col=1,cols do
			self.spaces[row][col] = nil
		end
	end
end

-- overrides

function Shelf:canAddItems(array)
    if #array > 1 then -- don't allow multiple parts to be added at once
        return false
    end
    if #array == 1 then
        return self:canAddItem(array[1])
    end
end

function Shelf:canAddItem(item)
    local row, col = self:getRowColAtGlobalXY(item:getCenter())
    if row and col then
        return self.spaces[row][col] == nil
    else
        return false
    end
end

function Shelf:addItem(item)
    local row, col = self:getRowColAtGlobalXY(item:getCenter())
    if row and col then
        self:addItemAtSpace(item, row, col)
        sounds.plopp:setPitch(math.random(80, 120)/100)
        sounds.plopp:play()
    else
        assert(false, "Can't add item here, did you check with canAddItem before?")
    end
end

function Shelf:getItemsAt(x, y)
    local row, col = self:getRowColAtGlobalXY(x, y)
    if row and col then
        return { self.spaces[row][col] }
    else
        return {}
    end
end

function Shelf:removeItem(item)
    assert(item.parent == self)
    sounds.pop:setPitch(math.random(80, 120)/100)
    sounds.pop:play()
    for row=1,self.rowCount do
		for col=1,self.colCount do
            if self.spaces[row][col] == item then
                customTransform, scale, rot = self:transformForChildAtSpace(item, row, col, baseScale)

                self.spaces[row][col] = nil
                self:stopBeingParent(item)

                if scale ~= baseScale or rot ~= 0 then
                    item:resetTransform()
                    item.transform = screen.cursor.transform * item.transform
                else
                    item.transform = self.transform * customTransform
                end
                return
			end
		end
    end
    assert(false, "Could not find item in spaces for removal.")
end

function Shelf:canAddItemAnywhere(item)
    return self:getEmptySpaceCount() >= 1
end

function Shelf:canAddItemsAnywhere(array)
    return self:getEmptySpaceCount() >= #array
end

function Shelf:addItemAnywhere(item)
    for n=1,100 do
        row = math.random(1, self.rowCount)
        col = math.random(1, self.colCount)
        if self.spaces[row][col] == nil then
            return self:addItemAtSpace(item, row, col)
        end
    end
    -- if random search did not work for some reason, try sequential
   for row=1,self.rowCount do
		for col=1,self.colCount do
            if self.spaces[row][col] == nil then
                return self:addItemAtSpace(item, row, col)
			end
		end
	end
    -- if the shelf is already full:
    return false
end

-- others

function Shelf:getRowColAtGlobalXY(x, y)
    x, y = self:getGlobalTransform():inverseTransformPoint(x, y)
    local row = self:findShelfRow(x, y)
    local col = self:findShelfCol(x, y)
    return row, col
end

function Shelf:getEmptySpaceCount()
    local count = 0
    for row=1,self.rowCount do
        for col=1,self.colCount do
            if self.spaces[row][col] == nil then
                count = count + 1
            end
        end
    end
    return count
end

-- place an item into the shelf at specified position
function Shelf:addItemAtSpace(item, row, col)
    assert(self.spaces[row][col] == nil, "Space not empty")
    self.spaces[row][col] = item
    self:becomeParent(item)
    item.transform = self:transformForChildAtSpace(item, row, col)
end

-- tries to put an item into the shelf in the place specified by pixel coordinates
function Shelf:addItemAtPixels(item, xPosInPx, yPosInPx)
    row = self:findShelfRow(xPosInPx, yPosInPx)
    col = self:findShelfCol(xPosInPx, yPosInPx)
    return self:addItemAtSpace(item, row, col)
end

-- finds out if the specified cordinates are inside the shelf
function Shelf:hereIsTheShelf(xPosInPx, yPosInPx)
    return self.rect:isPointInside(xPosInPx, yPosInPx)
end

-- finds out the row of the shelf at the specified cordinates
function Shelf:findShelfRow(xPosInPx, yPosInPx)
    if not self:hereIsTheShelf(xPosInPx, yPosInPx) then
        return false
    end
    local row = math.ceil((yPosInPx - self.rect.top)/self.shelfSpaceHeightInPx)
    return row
end

-- finds out the column of the shelf at the specified cordinates
function Shelf:findShelfCol(xPosInPx, yPosInPx)
    if not self:hereIsTheShelf(xPosInPx, yPosInPx) then
        return false
    end
    local col = math.ceil((xPosInPx - self.rect.left)/self.shelfSpaceWidthInPx)
    return col
end

function Shelf:transformForChildAtSpace(item, row, col, fixedScale)
    local x = (col-0.5)*self.shelfSpaceWidthInPx
    local y = (row-0.2)*self.shelfSpaceHeightInPx
    local w = item.image:getWidth()
    local h = item.image:getHeight()
    local long = w/h > 2.5 or w/h < 1/2.5
    local long = w/h > 2.5 or w/h < 1/2.5
    local small = w < 250 and h < 250
    local large = w > 600 or h > 600
    local scale = 0.3
    if small then
        scale = 0.6
    end
    if large then
        scale = 0.15
    end

    local rot = 0
    if long and not small then
        rot = math.pi/4
    end

    if fixedScale then
        scale = fixedScale
    end

    --scale = 0.1

    local t_center = love.math.newTransform(-w/2,-h/2)
    local t_down = love.math.newTransform(0,-h/2)
    local t_rot    = love.math.newTransform(0,0,rot)
    local t_center = love.math.newTransform(-w/2,-h/2)
    local t_scale  = love.math.newTransform(0,0,0,scale,scale)
    local t_move   = love.math.newTransform(x,y)
    --return t_center * t_rot * t_scale * t_move
    --return t_move * t_scale * t_center * t_rot
    return t_move * t_scale * t_down * t_rot * t_center, scale, rot
end
