--[[
A sculpture is composed of multiple objects.

The player selects, moves and rotates either single objects, or an object with appendages.

When a part of the sculpture is clicked, collision detection must be used to find the uppermost matching object. Then, the object is removed from the sculpture, but left in place. Each object in the sculpture is checked for connections to the root. Each object that is no longer connected to the root will be removed from the sculpture and added to the selection. Transforms will be made relative.

Z-Order should remain constant when removing / adding composed stuff. If an object or composition is added to the sculpture, it should get the highest z-order. Z-order is implicit by placing objects inside arrays, no explicit z-value is kept.

Building blocks that we need:
 
 * detect collisions between mouse coordinates and objects within sculptures or shelves (DONE)
 * detect if two objects (usually one within the sculpture, one within the selection) overlap sufficiently
 * move single objects between selection and sculpture, updating the transform (done in one direction)
 * coordinate the whole moving of multiple objects

 With transforms, we can easily convert pixel coordinates from one sprite to another.
]]

require "scenegraph.item"
require "scenegraph.itemgroup"
require "scenegraph.spatialitemgroup"
inspect = require "lib.inspect"
local class = require "lib.middleclass"

minOverlap = 5 -- in percent

-- helper functions
function createItemWithTransform(name, x ,y , degrees) 
    item = MaterialItem:new(name)
    item.transform = love.math.newTransform(x, y, math.rad(degrees))
    return item
end

function createEmptySculpture()
    local sculpture = Sculpture:new(Rect:new(-750, -1500, 750, 25))
    sculpture.transform = love.math.newTransform(800, 900, 0, 0.6, 0.6)
    return sculpture
end

-- sculpture
Sculpture = class("Sculpture", SpatialItemGroup)


-- constructor

function Sculpture:initialize(rect)
    ItemGroup.initialize(self, rect)
    self.isSculpture = true
    self.shape = nil
    self.root = nil
end

-- overrides

function Sculpture:canAddItem(item)
    return self.rect:isRectInside(item:getBoundingBox(self:getGlobalTransform()))
end

function Sculpture:canAddItems(array)
    for k, item in pairs(array) do
        if not self.rect:isRectInside(item:getBoundingBox(self:getGlobalTransform())) then
            return false
        end
    end

    return self:enoughOverlap(self.children, array)
    -- TODO: check overlap with sculpture 
end

function Sculpture:addItem(item)
    sounds.plopp:setPitch(math.random(80, 120)/100)
    sounds.plopp:play()
    SpatialItemGroup.addItem(self, item)
    self:resetCaches()
end

function Sculpture:removeItem(item)
    sounds.pop:setPitch(math.random(80, 120)/100)
    sounds.pop:play()
    SpatialItemGroup.removeItem(self, item)
    self:resetCaches()
end

-- checks it a set of new parts has enough overlap with the parts which are already fixed
-- for this, we assume that the new parts are sufficiently overlapping each other, so
-- that only one of the new parts must actually overlap the fixed ones.
function Sculpture:enoughOverlap(fixedParts, newParts)
    local overlap = 0
    local pixelCount = 0
    for cKey, cValue in pairs(newParts) do
        pixelCount = pixelCount + cValue:getPixelCount()
        for sKey, sValue in pairs(fixedParts) do
            overlap = overlap + cValue:getPixelOverlap(sValue)
        end
        if self.root then
            overlap = overlap + cValue:getPixelOverlap(self.root)
        end
        overlap = math.floor(overlap / pixelCount * 100)
        if overlap > minOverlap then
            return true
            -- todo make cheap computations more often, exit loops earlier
        end
    end
    return false
end

--function Sculpture:removeItems(array)
--    assert(false, "TODO")
--end

function Sculpture:drawChildren()
    love.graphics.setColor(1,1,1,1)
    if self.shape then
        self.shape:draw()
    end
    love.graphics.setColor(1,1,1,1)
    if self.root then
        self.root:draw()
    end
    love.graphics.setColor(1,1,1,1)
    ItemGroup.drawChildren(self)
end

function Sculpture:setRoot(rootItem)
    self.root = rootItem
    rootItem.parent = self
    self:resetCaches()
end

function Sculpture:setShape(shapeItem)
    self.shape = shapeItem
    self.shape.transform:translate(0, -150)
    shapeItem.parent = self
    self:resetCaches()
end

--calculates the overall rating for shape coverage, which is defined as (portion of shape is covered) - (portion of items which are outside the shape)
function Sculpture:getShapeCoverage()
    if self.shapeCoverageScore == nil then

        local shapePixel = self.shape:getPixelCount()
        local shapeCovered = self.shape:getPixelOverlapByMultiple(self.children)
        local outside = 0
        for i, child in pairs(self.children) do
            local currentOverlap = child:getPixelOverlap(self.shape)
            local currentOutside = child:getPixelCount() - currentOverlap
            outside = outside + currentOutside
        end
        --print("shapePixel: " .. shapePixel .. ", covered: " .. shapeCovered .. ", outside: " .. outside)
        self.shapeCoverageScore = (shapeCovered / shapePixel) - (outside / shapePixel) / 2 
        --print("Recomputed coverage")
    end
    return self.shapeCoverageScore
end

function Sculpture:resetCaches()
    self.shapeCoverageScore = nil
    self.selectionCache = {}
    --print("Reset caches.")
end

-- when a single item is selected for pickup, this function detects which other items must be picked up as well
function Sculpture:extendSelection(firstItem)

    local selection = self.selectionCache[firstItem]
    if selection == nil then
    
        selection = { firstItem}
        -- initially mark only those children which are not the root and not the clicked child, but directly touch the root
        for i, child in pairs(self.children) do
            child.isRooted = false
            if child ~= firstItem and child:getPixelOverlap(self.root) > child:getPixelCount() * minOverlap / 100 then
                child.isRooted = true
            end
        end

        repeat
            newRootedChildren = false
            -- now go through all unrooted children (except the clicked one) and check if they touch a rooted child
            for i, childPotUnrooted in pairs(self.children) do
                if childPotUnrooted ~= firstItem and not childPotUnrooted.isRooted then
                    for j, childPotRooted in pairs(self.children) do
                        if childPotRooted.isRooted and childPotUnrooted:getPixelOverlap(childPotRooted) > childPotUnrooted:getPixelCount() * minOverlap / 100 then
                            childPotUnrooted.isRooted = true
                            newRootedChildren = true
                        end
                    end
                end
            end
            -- repeat as long as there are new children which are marked as rooted in this turn
        until newRootedChildren == false

        -- when the root-markings are stable, add all children which are not marked as rooted to the selection
        for i, child in pairs(self.children) do
            if child ~= firstItem and not child.isRooted then
                table.insert(selection, child)
            end
        end
        self.selectionCache[firstItem] = selection
        --print("New selection for " .. firstItem.name)
    end

    return selection
end

function Sculpture:getItemsAt(mouseX, mouseY) 
    local directSelection = SpatialItemGroup.getItemsAt(self, mouseX, mouseY)
    if #directSelection > 0 then
        return self:extendSelection(directSelection[1])
    end

    return { }
end
