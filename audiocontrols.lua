-- this displays a mute button for all audio and volume controls for the music
muteButtonPosX = 20
muteButtonPosY = 20
muteButtonWidth = 50
muteButtonHeight = 50

volumeControlBoxPosX = 80
volumeControlBoxPosY = 20
volumeControlBoxWidth = 130
volumeControlBoxHeight = 50

function drawAudioControls()
    
    love.graphics.push() 

    local x, y = love.mouse.getPosition()

    -- draw mute button: -------------------

    -- -- draw button shape
    if inBox(x, y, muteButtonPosX, muteButtonPosY, muteButtonWidth, muteButtonHeight) then
        love.graphics.setColor(0.9, 0.7, 0.5, 1)
    else
        love.graphics.setColor(0.7, 0.5, 0.3, 1)
    end
    love.graphics.rectangle("fill", muteButtonPosX, muteButtonPosY, muteButtonWidth, muteButtonHeight, buttonCornerRadius)
    love.graphics.setColor(0.1, 0.1, 0.1, 1)
    love.graphics.rectangle("line", muteButtonPosX, muteButtonPosY, muteButtonWidth, muteButtonHeight, buttonCornerRadius)

    -- -- set symbol
    local symbol = nil
    if love.audio.getVolume() > 0.0 then
        symbol = images["sound_on"]
    else
        symbol = images["sound_off"]
    end
    -- -- draw symbol
    local symbolScalingFactor = (30/symbol:getHeight())
    love.graphics.draw(symbol, muteButtonPosX+10, muteButtonPosY+10, 0, symbolScalingFactor, symbolScalingFactor)

    -- draw volume controls: ----------------

    -- -- draw box
    love.graphics.setLineWidth(2)
    love.graphics.setColor(0.8, 0.6, 0.4, 1)
    love.graphics.rectangle("fill", volumeControlBoxPosX, volumeControlBoxPosY, volumeControlBoxWidth, volumeControlBoxHeight, buttonCornerRadius)
    love.graphics.setColor(0.1, 0.1, 0.1, 1)
    love.graphics.rectangle("line", volumeControlBoxPosX, volumeControlBoxPosY, volumeControlBoxWidth, volumeControlBoxHeight, buttonCornerRadius)

    -- -- draw minus button
   
    if inBox(x, y, volumeControlBoxPosX, volumeControlBoxPosY, volumeControlBoxHeight, volumeControlBoxHeight) then
        love.graphics.setColor(0.9, 0.7, 0.5, 1)
    else
        love.graphics.setColor(0.7, 0.5, 0.3, 1)
    end
    love.graphics.rectangle("fill", volumeControlBoxPosX, volumeControlBoxPosY, volumeControlBoxHeight, volumeControlBoxHeight, buttonCornerRadius)
    love.graphics.setColor(0.1, 0.1, 0.1, 1)
    love.graphics.rectangle("line", volumeControlBoxPosX, volumeControlBoxPosY, volumeControlBoxHeight, volumeControlBoxHeight, buttonCornerRadius)

    symbol = images["minus"]
    symbolScalingFactor = (30/symbol:getHeight())
    love.graphics.draw(symbol, volumeControlBoxPosX+10, volumeControlBoxPosY+10, 0, symbolScalingFactor, symbolScalingFactor)

    -- -- draw plus button
   
    if inBox(x, y, volumeControlBoxPosX+volumeControlBoxWidth-volumeControlBoxHeight, volumeControlBoxPosY, volumeControlBoxHeight, volumeControlBoxHeight) then
        love.graphics.setColor(0.9, 0.7, 0.5, 1)
    else
        love.graphics.setColor(0.7, 0.5, 0.3, 1)
    end
    love.graphics.rectangle("fill", volumeControlBoxPosX+volumeControlBoxWidth-volumeControlBoxHeight, volumeControlBoxPosY, volumeControlBoxHeight, volumeControlBoxHeight, buttonCornerRadius)
    love.graphics.setColor(0.1, 0.1, 0.1, 1)
    love.graphics.rectangle("line", volumeControlBoxPosX+volumeControlBoxWidth-volumeControlBoxHeight, volumeControlBoxPosY, volumeControlBoxHeight, volumeControlBoxHeight, buttonCornerRadius)

    symbol = images["plus"]
    symbolScalingFactor = (30/symbol:getHeight())
    love.graphics.draw(symbol, volumeControlBoxPosX+volumeControlBoxWidth-volumeControlBoxHeight+10, volumeControlBoxPosY+10, 0, symbolScalingFactor, symbolScalingFactor)

    -- -- draw notes symbol
    
    symbol = images["notes"]
    symbolScalingFactor = (30/symbol:getHeight())
    love.graphics.draw(symbol, volumeControlBoxPosX+(volumeControlBoxHeight)+2, volumeControlBoxPosY+10, 0, symbolScalingFactor, symbolScalingFactor)

    -- end of volume controls ---------------

    love.graphics.pop()
end

function clickAudioControls()
    
    local x, y = love.mouse.getPosition()
    local vol = love.audio.getVolume()

    if inBox(x, y, muteButtonPosX, muteButtonPosY, muteButtonWidth, muteButtonHeight) then
        if (vol > 0.0) then
            love.audio.setVolume(0.0)
        elseif (vol == 0) then
            love.audio.setVolume(0.5)
        end
        return true
    end

    if inBox(x, y, volumeControlBoxPosX, volumeControlBoxPosY, volumeControlBoxHeight, volumeControlBoxHeight) then
        if (vol > 0.0) then
            love.audio.setVolume(vol-0.1)
        end
        return true
    end
   
    if inBox(x, y, volumeControlBoxPosX+volumeControlBoxWidth-volumeControlBoxHeight, volumeControlBoxPosY, volumeControlBoxHeight, volumeControlBoxHeight) then
        if (vol < 0.9) then
            love.audio.setVolume(vol+0.1)
        end
        return true
    end
    return false
end
