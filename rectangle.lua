local class = require "lib.middleclass"

-- Rect
Rect = class("Rect")

function Rect:initialize(left, top, right, bottom)
    self.left = left
    self.right = right
    self.top = top
    self.bottom = bottom
end

function Rect:isPointInside(x, y)
    return x >= self.left and x <= self.right and y >= self.top and y <= self.bottom
end


function Rect:isRectInside(other)
   return other.left >= self.left and
           other.right <= self.right and
           other.bottom <= self.bottom and
           other.top >= self.top
end

function Rect:intersects(other)
    if self.left > other.right or self.right < other.left then
        return false
    end
    if self.top > other.bottom or self.bottom < other.top then
        return false
    end
    return true
end

function Rect:draw()
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle("line", self.left, self.top, self.right - self.left, self.bottom - self.top)
end

function Rect:getWidth()
    return self.right - self.left
end

function Rect:getHeight()
    return self.bottom - self.top
end

function Rect:extractTransform()
    local transform = love.math.newTransform(self.left, self.top)
    self.bottom = self.bottom - self.top
    self.right = self.right - self.left
    self.left = 0
    self.top = 0
    return transform
end

function Rect:tostring()
    return "rect at " .. self.left .. "," .. self.top .. " to " .. self.right .. "," .. self.bottom
end

function Rect:includePoint(x,y)
    if not self.left then
        self.left   = x
        self.right  = x
        self.top    = y
        self.bottom = y
    else
        self.left   = math.min(self.left, x)
        self.right  = math.max(self.right, x)
        self.top    = math.min(self.top, y)
        self.bottom = math.max(self.bottom, y)
    end
end

function Rect:getCenter()
    return (self.left + self.right) / 2, (self.top + self.bottom) / 2
end