--global variables:
require "scenegraph.screen"

orders = {
    -- explain basic controls
    -- let player build stuff to reach a certain weight
    {
        intro="Good morning! I heard you're the new owner of »Arden's Artisan Artifacts«? Then you surely know that you can click on items to pick them up, and stick them to other things on the table when they have enough overlap. Drag or use your mousewheel to rotate them. Can you build me a heavy sacrifice statue, please?",
        positive="Oh wow, I barely can lift it! I'm sure the God of Gravity will be satisfied! Here, I want you to have this book as a reward!",
        negative="Um, are you sure this statue is heavy enough? Oh well, at least I'll be able to carry it home...",
        conditions={
            heavy={min=3},
        },
        rewards={"book_of_evil"},
        customer="old_cultist",
    },
    -- explain clutter box
    -- collect items of a certain property, which is not too common
    {
        intro="I... uh... I think there's a monster under my bed! My brother said... if we don't sacrifice something creepy to it, it will eat me in the night! Can you help me? Maybe you'll find something useful in your clutter box?",
        positive="That looks... horrible! Perfect! I will burn it this evening, and I hope the monster will finally leave me alone! Here, take this flower. And my teddy bear, I don't think I will need it anymore.",
        negative="I hope that will be enough to... satisfy the monster\'s hunger... *shudder*",
        conditions={
            creepy={min=5},
        },
        rewards={"teddy", "sunflower"},
        customer="tiny_cultists",
    },
    -- introduce shape constraint
    {
        intro="Oh no! I totally forgot that I also have a math test tomorrow! Hopefully, a sacrifice to the Goddess of Polynomials will calm her! I once saw this cryptic glyph in a math book - can you make the artifact look like it?",
        positive="\"The product of any collection of compact topological spaces is compact with respect to the product topology!\" Huh... I think it worked! Here, take this alarm clock! I'm sure the Goddess will wake me up when it's time to get up.",
        negative="Ugh, math is so boring! Who needs stuff like that anyway?",
        conditions={
        },
        shape="pi",
        root="ceiling",
        rewards={"alarm"},
        customer="tiny_cultists",
    },
    -- two properties common in many objects: have much of one, but little of the other
    {
        intro="Good afternoon! I'm here to pick up the weekly routine delivery for my boss, the God of Mondays. You remember that he likes very, very hard things? And don't make it so shiny this time.",
        positive="Thanks. Your work is always appreciated. Have this mug, it even contains our company logo!",
        negative="I don't think I will come back. Good day to you.",
        conditions={
            hard={min=5},
            shiny={max=1},
        },
        rewards={"mug"},
        customer="boring_customer",
    },
    -- a rare property + a shape constraint
    {
        intro="I need the mercy of the Sea Goddess so that I will catch a lot of fish. Could you make me a sacrifice shaped like a big fish, please? And I think it would be very helpful if it included a lot of parts that come from the sea.",
        positive="Wow, I think she will love that! Do you like feathers? You can have these!",
        negative="Thanks! I hope this will be good enough to gain her mercy :/",
        conditions={
            maritime={min=6},
        },
        rewards={"feather", "feather", "feather"},
        customer="grumpy_cultist",
        shape="fish",
        root="ceiling",
    },
    -- two opposed properties + a shape constraint
    {
        intro="I need your help! I think the Tooth Fairies are angry with me! I need a light, fluffy statue to calm them.",
        positive="Thanks! I think the Tooth Fairies will leave me alone now. I didn't think they were real, until today. I even collected all these coins for them. But you can have them now.",
        negative="Huh, that doesn't look very light and fluffy. Maybe I should just go to the dentist instead.",
        conditions={
            heavy={max=2},
            fluffy={min=3},
        },
        rewards={"coins"},
        customer="creepy_cultist",
        shape="flower"
    },
    {
        intro="Hello again! Our cultist training begins tomorrow, so we need something safe to train with! It should not be dangerous in any way, so we won't hurt ourselves. What do you think?",
        positive="This will be great! Our teachers will love it! Thanks a lot! Oh, and we found these things on our way here, do you want to keep them? So creepy! Ew!",
        negative="Uh-oh. We're not sure we will be allowed to bring this to cultist school.",
        conditions={
            spiky={max=1.6},
            creepy={max=1.6},
            poisonous={max=1.6},
        },
        rewards={"finger", "eyeball", "eyeball"},
        shape="snake",
        customer="tiny_cultists",
    },
    -- hardest challenge! maximize a lot of things, complicated shape
    {
        intro="Oh my, oh my, I'm in trouble. Can you build me a poisonous, spiky thingamajig, which looks a bit like it's a giant, alive spider? Please don't ask. And yeah, I know you're closing soon, but I need it right now!",
        positive="Perfect! I'm in your debt forever! What? No... please don't ask why I need it. Ugh. Huh, do you have any use for half a bottle of poison? I have enough of it.",
        negative="Oh my, oh my. That won't do. That won't do at all. Wish me luck. Oh my.",
        conditions={
            poisonous={min=2},
            spiky={min=4},
            alive={min=4},
        },
        rewards={},
        customer="creepy_cultist",
        shape="spider",
        root="ceiling",
    },
    -- closing words
    -- interesting, but rather easy final challenge
    {
        intro="Oh, I think you are closing soon, right? But I have a final order for the day: Can you build me a homunculus?",
        positive="Cute! Maybe I'll keep it! Anyway, have a nice evening! I think you made many people happy today!",
        negative="Huh. Is that really what a homunculus looks like? But thanks! I like being your customer! Bye!",
        conditions={
            heavy={max=1},
            human={min=2},
        },
        rewards={},
        shape="homunculus",
        customer="old_cultist",
    },
}

customerShift = -550
customerTarget = 0
customerX = 70
customerY = 540
customerWidth = 150
customerHeight = 400

boxShift = -2000
boxShown = false
orderBoxCornerRadius = 20
orderBoxPositionX = 230
orderBoxPositionY = 20
orderBoxWidth = 1000
orderBoxHeight = 450

buttonWidth = 300
buttonHeight = 70
buttonPositionX = orderBoxPositionX+orderBoxWidth-buttonWidth-20
buttonPositionY = orderBoxPositionY+orderBoxHeight-buttonHeight-20
buttonCornerRadius = 10

secondButtonPositionX = orderBoxPositionX+orderBoxWidth-2*buttonWidth-40
secondButtonPositionY = orderBoxPositionY+orderBoxHeight-buttonHeight-20

--functions:

currentOrderNumber = 0

function nextOrder()
    currentOrderNumber = currentOrderNumber + 1
    local neworder = startNewOrder(currentOrderNumber)
    if not neworder then
        gameState = "ending"
    end
end

function startRandomOrder()
    startNewOrder(math.random(#orders))
end

-- sets the order to a new value (boolean) 
function startNewOrder(nr)
    if nr > #orders then
        return false
    end
    
    order = orders[nr]
    order.state = "new"
    customerShift = -550
    customerTarget = 0
    
    timeToShowBox = love.timer.getTime() + 1.5

    initOrderWithStats()
    return true
end

-- initialize all orders with number and sculpture
function initOrders()
    for i, order in pairs(orders) do
        order.sculpture = createEmptySculpture()

        local root = "desk"
        if order.root then
            root = order.root
        end

        local rootItem = Item:new("root_"..root)

        if root == "ceiling" then
            rootItem.transform:translate(0, -650)
        end

        order.sculpture:setRoot(rootItem)
        order.nr = i
    end
end

--creates success measurements for the order conditions, sets the shape, ...
function initOrderWithStats()
    --find out which conditions should be measured:
    order.measurements = {}
    for property, range in pairs(order.conditions) do
        order.measurements[property] = range
        --set max length of progress bar
        if order.measurements[property].min ~= nil then
            order.measurements[property].length = (order.measurements[property].min * 1.5)
        end
        if order.measurements[property].max ~= nil then
            if order.measurements[property].max == 0 then
                order.measurements[property].length = 2
            else
                order.measurements[property].length = (order.measurements[property].max * 3)
            end
        end
    end
    if order.shape ~= nil then
        local shapeImageName = "shape_"..order.shape
        order.sculpture:setShape(ShapeItem:new(shapeImageName))
        
        -- love.math.newTransform(-order.shapeImage:getWidth() * scaleFactor / 2, 100 - targetShapeHeight, 0, scaleFactor, scaleFactor)
        order.measurements["shape"] = {min=0.7, length=1}
    end
end

--draws the success meters for the current order
function drawOrderMeters()
    if order == nil or screen.sculpture == nil then
        return
    end

    local i = 1
    for name, range in pairs(order.measurements) do
        local barWidth = 300
        local barHeight = 70
        local barPositionY = CANVAS_HEIGHT-120
        local barPositionX = 350*(i+1) - 200
        local cornerRadius = 10
        local innerBarHeight = 20
        local innerBarPositionY = (barPositionY + barHeight) - (innerBarHeight + 10)
        love.graphics.setLineWidth(2)
        --draw background box
        love.graphics.setColor(0.8, 0.6, 0.4, 1)
        love.graphics.rectangle("fill", barPositionX, barPositionY, barWidth, barHeight, cornerRadius)
        love.graphics.setColor(0.1, 0.1, 0.1, 1)
        love.graphics.rectangle("line", barPositionX, barPositionY, barWidth, barHeight, cornerRadius)
        --calculate size of the progress bar
        local meter = order.measurements[name]
        local length = meter.length
        local meterValue = getMeasurementValue(name)
        --draw progress bar with background:--------------
        local progress = math.min((meterValue/length)*(barWidth-20), (barWidth-20))
        if progress < 0 then
            progress = 0
        end
        local progressCornerRadius = cornerRadius - 3
        --set color and symbol (green & check if fulfilled, red & cross if not fulfilled)
        local symbol = nil
        if conditionIsFulfilled(name, meter) then
            love.graphics.setColor(0, 1, 0, 1)
            symbol = images["check"]
        else
            love.graphics.setColor(0.7, 0, 0, 1)
            symbol = images["cross"]
        end
        --draw real progress bar
        if progress == 0 then
            progressCornerRadius = 0
        end
        love.graphics.rectangle("fill", barPositionX+10, innerBarPositionY, progress, innerBarHeight, progressCornerRadius)
        local symbolScalingFactor = (30/symbol:getHeight())
        love.graphics.draw(symbol, (barPositionX+barWidth)-40, barPositionY+5, 0, symbolScalingFactor, symbolScalingFactor)
        --draw progress bar background
        progressCornerRadius = cornerRadius - 3
        love.graphics.setColor(0.1, 0.1, 0.1, 1)
        love.graphics.rectangle("line", barPositionX+10, innerBarPositionY, (barWidth-20), innerBarHeight, progressCornerRadius)
        --draw name of the measurement
        love.graphics.setColor(0.1, 0.1, 0.1, 1)
        love.graphics.setFont(tinyfont)
        love.graphics.print(name, (barPositionX+10), barPositionY+5)
        love.graphics.setFont(mainfont)
        --increment counter variable
        i = i+1
        --reset color to default
        love.graphics.setColor(1, 1, 1, 1)
    end
end

--checks if a specific condition is fulfilled (boolean)
function conditionIsFulfilled(name, range)
    if range.max and getMeasurementValue(name) > range.max then
        return false
    end
    if range.min and getMeasurementValue(name) < range.min then
        return false
    end
    return true
end

--calculates the current value of a condition meter (float)
function getMeasurementValue(name)
    if name=="shape" then
        return order.sculpture:getShapeCoverage()
    else
        --calculate the specified property for the current sculpture
        local sum = 0
        for index, child in pairs(screen.sculpture.children) do
            sum = sum + child.properties[name]
        end
        return sum
    end
end

--checks if the conditions of an order are fulfilled (boolean)
function isOrderFulfilled()
    for property, range in pairs(order.conditions) do
        if not conditionIsFulfilled(property, range) then
            return false
        end
    end
    if order.shape ~= nil then
        if order.sculpture:getShapeCoverage() < 0.5 then
            return false
        end
    end
    return true
end

--deliver the current order to the customer and get a response
function handInCurrentOrder()
    if order.state == "current" then
        if #screen.sculpture.children == 0 then
            -- the user surely misclicked
            return
        end
        sounds.deliver:setPitch(math.random(80, 120)/100)
        sounds.deliver:play()
        if isOrderFulfilled() then
            order.state = "positive"
            if order.rewards then
                for i,name in pairs(order.rewards) do
                    screen.shelf:addItemAnywhere(MaterialItem:new(name))
                end
            end
        else
            order.state = "negative"
        end
    end
end

-- check if the cursor is on the customer
function onCustomer(x, y)
    --return inBox(x, y, customerX-customerWidth/2, customerY-customerHeight/2, customerWidth, customerHeight)
    return inBox(x, y, 0, 300, 300, 1080-300)
end

-- check if the cursor is on the button
function onButton(x, y)
    return inBox(x, y, buttonPositionX, buttonPositionY, buttonWidth, buttonHeight)
end

-- check if the cursor is on the button
function onSecondButton(x, y)
    return inBox(x, y, secondButtonPositionX, secondButtonPositionY, buttonWidth, buttonHeight)
end

-- toggle if the order box is displayed
function toggleOrderBox()
    if order == nil then
        return
    end
    boxShown = not boxShown
    if boxShown then
        customerTarget = 0
    else
        customerTarget = -150
    end
    if order.state == "new" then
        screen:setSculpture(order.sculpture)
        order.state = "current"
    end
end

-- animation for sliding the order box in and out
function updateOrderBox(dt)
    if boxShown and boxShift < 0 then
        boxShift = boxShift + 10000*dt
    elseif not boxShown and boxShift > -2000 then
        boxShift = boxShift - 10000*dt
    end

    if boxShift > 0 then
        -- oops, we overshot!
        boxShift = 0
    end
end

-- animation for sliding the customer in and out
function updateCustomerPosition(dt)
    if customerShift < customerTarget then
        customerShift = customerShift + 1000*dt
        if customerShift > customerTarget then -- oops, we overshot!
            customerShift = customerTarget
        end
    elseif customerShift > customerTarget then
        customerShift = customerShift - 1000*dt
        if customerShift < customerTarget then -- oops, we overshot!
            customerShift = customerTarget
        end
    end   
end

-- draws the order box (the customer's speech bubble)
function drawOrderBox()
    if order then
        local text = order.intro
        love.graphics.push()
            --apply box shift (for animation)
            love.graphics.translate(boxShift, 0)
            --draw order box with outline
            love.graphics.setLineWidth(2)
            love.graphics.setColor(0.8, 0.6, 0.4, 1)
            love.graphics.rectangle("fill", orderBoxPositionX, orderBoxPositionY, orderBoxWidth, orderBoxHeight, orderBoxCornerRadius)
            love.graphics.setColor(0.1, 0.1, 0.1, 1)
            love.graphics.rectangle("line", orderBoxPositionX, orderBoxPositionY, orderBoxWidth, orderBoxHeight, orderBoxCornerRadius)
            ---draw text
            love.graphics.setFont(mainfont)
            love.graphics.printf(text, orderBoxPositionX + 20, orderBoxPositionY + 20, orderBoxWidth-40)
            love.graphics.setColor(1, 1, 1)

            -- draw buttons
            drawButton()
            if order.state == "current" then
                drawSecondButton()
            end

            text = ""
            if order.state == "current" then
                if isOrderFulfilled() then
                    --text = "That looks really good!"
                else
                    --text = "The current statue is still missing something..."
                end
            elseif order.state == "positive" then
                text = order.positive
            elseif order.state == "negative" then
                text = order.negative
            end
            love.graphics.printf(text, orderBoxPositionX + 20, orderBoxPositionY + 220, orderBoxWidth-40)
        love.graphics.pop()
    end
end

-- draws the customer (position animated)
function drawCustomer()
    if order and order.customer then
        local x, y = love.mouse.getPosition()

        local text = order.intro
        love.graphics.push()
            --apply box shift (for animation)
            love.graphics.translate(customerShift, -customerShift)
            if onCustomer(x, y) then
                effect:send("colorMatrix", lighterMatrix)
            else
                effect:send("colorMatrix", identityMatrix)
            end
            love.graphics.setShader(effect)
            love.graphics.draw(images[order.customer], customerX, customerY, 0, 0.2, 0.2)
            love.graphics.setShader()

        love.graphics.pop()
    end
end

-- draws the button
function drawButton()
    local x, y = love.mouse.getPosition()

    --draw rounded rectangle with outline
    love.graphics.setLineWidth(2)
    if onButton(x, y) then
        love.graphics.setColor(0.9, 0.7, 0.5, 1)
    else
        love.graphics.setColor(0.7, 0.5, 0.3, 1)
    end
    love.graphics.rectangle("fill", buttonPositionX, buttonPositionY, buttonWidth, buttonHeight, buttonCornerRadius)
    love.graphics.setColor(0.1, 0.1, 0.1, 1)
    love.graphics.rectangle("line", buttonPositionX, buttonPositionY, buttonWidth, buttonHeight, buttonCornerRadius)
    ---draw text
    local text = "???"
    if order.state == "current" then
        text = "I'm done"
    elseif order.state == "new" then
        text = "Start working"
    else
        text = "Next customer"
    end

    love.graphics.printf(text, buttonPositionX + 10, buttonPositionY + 15, buttonWidth-20, "center")
    love.graphics.setColor(1, 1, 1)
end

-- draws the second button
function drawSecondButton()
    local x, y = love.mouse.getPosition()

    --draw rounded rectangle with outline
    love.graphics.setLineWidth(2)
    if onSecondButton(x, y) then
        love.graphics.setColor(0.9, 0.7, 0.5, 1)
    else
        love.graphics.setColor(0.7, 0.5, 0.3, 1)
    end
    love.graphics.rectangle("fill", secondButtonPositionX, secondButtonPositionY, buttonWidth, buttonHeight, buttonCornerRadius)
    love.graphics.setColor(0.1, 0.1, 0.1, 1)
    love.graphics.rectangle("line", secondButtonPositionX, secondButtonPositionY, buttonWidth, buttonHeight, buttonCornerRadius)
    ---draw text
    local text = "Continue working"
    love.graphics.printf(text, secondButtonPositionX + 10, secondButtonPositionY + 15, buttonWidth-20, "center")
    love.graphics.setColor(1, 1, 1)
end

-- this function is called in mouserelased when the button is clicked
function buttonClicked()
    if order == nil then
        return
    elseif order.state == "current" then
        handInCurrentOrder()
    elseif order.state == "new" then
        toggleOrderBox()
        screen:setSculpture(order.sculpture)
    else
        nextCustomerTime = love.timer.getTime() + 1
        customerTarget = -550
        boxShown = false
        -- order = nil
        screen:setSculpture(nil)
        sounds.door:setPitch(math.random(80, 120)/100)
        sounds.door:play()
        --nextOrder()
    end
end
