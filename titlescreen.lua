gameTitle = "Arden's Artisan Artifacts"
subtitle = "Sustainable Handmade Sacrifice Sculptures for all Occasions"

titlePosX = 0
titlePosY = 300
titleWidth = CANVAS_WIDTH

--this draws all elements of the title screen
function drawTitleScreen()
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(images.title_without, 0,0,0,0.5,0.5)
    love.graphics.setFont(bigfont)
    love.graphics.setColor(0.82, 0.75, 0.66, 1)
    if finishedLoadImages then
        love.graphics.print("Click to start playing", 94, 750)
    else
        love.graphics.print("Loading…", 94, 750)
    end
end
