images = {}
imageDatas = {}
sounds = {}
music = {}
fonts = {}

function loadAssets()
    for i,filename in pairs(love.filesystem.getDirectoryItems("images/other/fullsize")) do
        if filename ~= ".gitkeep" and filename ~= ".DS_Store" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/other/fullsize/"..filename, { mipmaps = true })
        end
    end

    for i,filename in pairs(love.filesystem.getDirectoryItems("images/clickable/fullsize")) do
        if filename ~= ".gitkeep" and filename ~= ".DS_Store" then
            imageData = love.image.newImageData("images/clickable/fullsize/"..filename, { mipmaps = true })
            image = love.graphics.newImage(imageData)
            imageDatas[filename:sub(1,-5)] = imageData
            images[filename:sub(1,-5)] = image
        end
    end

    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" and filename ~= ".DS_Store" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
            sounds[filename:sub(1,-5)]:setVolume(0.15)
        end
    end
    sounds.plopp:setVolume(0.3)
    sounds.searching:setVolume(0.1)
    sounds.deliver:setVolume(0.05)

    mainfont = love.graphics.newFont("fonts/imfellenglish.ttf", 32)
    tinyfont = love.graphics.newFont("fonts/imfellenglish.ttf", 26)
    bigfont = love.graphics.newFont("fonts/imfellenglish.ttf", 42)
    oldfont = love.graphics.newFont("fonts/oldenglishfive.ttf", 30)
    bigoldfont = love.graphics.newFont("fonts/oldenglishfive.ttf", 60)
    love.graphics.setFont(mainfont)
end
