endtitlePosX = 0
endtitlePosY = 20
endtitleWidth = CANVAS_WIDTH

EndScreenPrepared = false
howManyOrders = #orders
howManyPositive = 0
ordersPerRow = math.ceil(howManyOrders/2)

-- preparation --
function prepareEndScreen()

    -- prepare all the orders -------------
    for i, order in pairs(orders) do
        -- count fulfilled orders:
        if order.state == "positive" then
            howManyPositive = howManyPositive+1
        end
        order.randomRotation = 0.06*math.random()-0.06*math.random()
        local rowNr = 0
        if i > ordersPerRow then
            rowNr = 1
        end
        order.posX = 100+(355*(i-1)-(rowNr*355*(ordersPerRow)))
        order.posY = 140+(500*rowNr)

        order.sculpture.transform = love.math.newTransform(0, 0, 0, 0.2, 0.2)
        order.sculpture.shape = nil
        
        if order.state == "positive" then
            order.symbol = images["check"]
        else
            order.symbol = images["cross"]
        end
        
    end
    endScreenPrepared = true
end

-- draws the pictures
function drawOrderPictures()
    for i, order in pairs(orders) do
        love.graphics.push()
        local scale = 0.05
        local customerImage = images[order.customer]
        
        -- set coordinate origin and rotate coordinate system
        love.graphics.translate(order.posX, order.posY)
        love.graphics.rotate(order.randomRotation)
        -- draw frame
        love.graphics.setColor(0.1, 0.1, 0.1, 1)
        love.graphics.rectangle("line", 0, 0, 300, 350)
        love.graphics.setColor(1, 0.9, 0.9, 1)
        love.graphics.rectangle("fill", 0, 0, 300, 350)

        -- draw evaluation symbol
        if order.state == "positive" then
            love.graphics.setColor(0, 1, 0, 1)
            love.graphics.draw(order.symbol, 250, 300, 0, 0.6, 0.6)
        else
            love.graphics.setColor(0.7, 0, 0, 1)
            love.graphics.draw(order.symbol, 250, 300, 0, 0.6, 0.6)
        end
        -- draw sculpture
        love.graphics.push()
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.translate(150, 320)
        order.sculpture:draw()
        love.graphics.pop()
        -- draw customer
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.draw(customerImage, -40, 380-(customerImage:getHeight()*scale), 0, scale, scale)
        -- reset coordinate system
        love.graphics.pop()
    end
end


--this draws all elements of the ending screen
function drawEndScreen()
    if not endScreenPrepared then
        prepareEndScreen()
    end

    -- draw background ------
    love.graphics.setBackgroundColor(0.8, 0.6, 0.4)

    -- draw text -----------
    love.graphics.setColor(0.1, 0.1, 0.1, 1)
    
    love.graphics.setFont(bigoldfont)
    love.graphics.printf("Thank you for playing!", endtitlePosX, endtitlePosY, endtitleWidth, "center")

    love.graphics.setFont(bigfont)
    love.graphics.printf(howManyPositive.." out of "..howManyOrders.." customers are very satisfied with your work!", endtitlePosX, (CANVAS_HEIGHT/2), endtitleWidth, "center")

    love.graphics.printf("Click to exit", endtitlePosX, CANVAS_HEIGHT-60, endtitleWidth-40, "right")

    drawOrderPictures()
end
