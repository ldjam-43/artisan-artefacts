require "lib.slam"
vector = require "lib.hump.vector"
tlfres = require "lib.tlfres"
inspect = require "lib.inspect"

CANVAS_WIDTH = 1920
CANVAS_HEIGHT = 1080
gameState = "title"

baseScale = 0.7

require "helpers"
require "itemCatalog"
require "orders"
require "assets"
require "rectangle"
require "scenegraph.shelf"
require "scenegraph.item"
require "scenegraph.sculpture"
require "scenegraph.screen"
require "exitdialog"
require "titlescreen"
require "endscreen"
require "audiocontrols"

order = nil -- current order

mouseStartX = 0
mouseStartY = 0

frameCount = 0

startedLoadImages = false
finishedLoadImages = false

function love.load()
    images["title_without"] = love.graphics.newImage("images/other/fullsize/title_without.jpg", { mipmaps = true })
    fonts["imfellenglish"] = {}
    fonts["imfellenglish"][42] = love.graphics.newFont("fonts/imfellenglish.ttf", 42)
    bigfont = fonts["imfellenglish"][42]

    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" and filename ~= ".DS_Store" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename, "stream")
            music[filename:sub(1,-5)]:setLooping(true)
        end
    end

    music.industriousferret:setVolume(0.1)
    music.industriousferret:play()


    math.randomseed(os.time())    
    -- set up default drawing options
    love.graphics.setBackgroundColor(0.5, 0.5, 0.5)
end

function postLoad()
    loadAssets()
 
    screen = Screen:new()

    screen.shelf:addItemAnywhere(MaterialItem:new("chandelier"))
    screen.shelf:addItemAnywhere(MaterialItem:new("broom1"))
    screen.shelf:addItemAnywhere(MaterialItem:new("voodoo_doll"))
    screen.shelf:addItemAnywhere(MaterialItem:new("book_of_evil"))
    
    for i=1,16 do
        screen.shelf:addItemAnywhere(createRandomItem())
    end

    
    initOrders()

    effect = love.graphics.newShader [[
        uniform mat4 colorMatrix;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
            vec4 texPoint = texture2D(texture, texture_coords);
            return vec4((colorMatrix * texPoint) * color);
        }
    ]]

    identityMatrix     =  {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}
    desaturationMatrix =  {{0.55, 0.55, 0.55, 0.33}, {0.33, 0.33, 0.33, 0}, {0.33, 0.33, 0.33, 0}, {0, 0, 0, 0.75}}
    lighterMatrix      =  {{1.33, 0.33, 0.33, 0}, {0.33, 1.33, 0.33, 0}, {0.33, 0.33, 1.33, 0}, {0, 0, 0, 1}}
    effect:send("colorMatrix", identityMatrix)

    finishedLoadImages = true
end

function love.update(dt)
    frameCount = frameCount + 1

    if not startedLoadImages and frameCount > 5 then
        startedLoadImages = true
        postLoad()
        return
    end

    if not finishedLoadImages then
        return
    end

    x, y = love.mouse.getPosition()
    if not mouse_button_pressed then
        screen.cursor:updatePosition(x, y)
    end

    if love.keyboard.isDown("left", "up") then
        screen.cursor:rotate(dt * -50)
    end 
    if love.keyboard.isDown("right", "down") then
        screen.cursor:rotate(dt * 50)
    end 

    updateOrderBox(dt)
    updateCustomerPosition(dt)

    if nextCustomerTime and love.timer.getTime() > nextCustomerTime then
        sounds.door:setPitch(math.random(80, 120)/100)
        sounds.door:setVolume(0.25)
        sounds.door:play()
        nextCustomerTime = nil
        nextOrderTime = love.timer.getTime() + 1
    end

    if nextOrderTime and love.timer.getTime() > nextOrderTime then
        nextOrder()
        nextOrderTime = nil
    end

    if timeToShowBox and love.timer.getTime() > timeToShowBox then
        boxShown = true
        timeToShowBox = nil
    end
end

function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

function love.keypressed(key)
    if not finishedLoadImages then
        return
    end

    if key == "escape" then
        showExitDialog()
    elseif key == "f" then
        isFullscreen = love.window.getFullscreen()
        love.window.setFullscreen(not isFullscreen)
    end
end

function love.keyreleased(key)
end

function love.mousepressed(x, y, button)
    --if not finishedLoadImages then
        --return
    --end

    if button == 1 then -- left/primary button
        mouse_button_pressed = true
        mouse_moved_distance = 0
        mouseStartX, mouseStartY = love.mouse.getPosition()
    end
end

function love.mousereleased(x, y, button)


    if button == 1 then -- left/primary button
        mouse_button_pressed = false

        if mouse_moved_distance < 10 then
            x, y = love.mouse.getPosition() --overwrite x and y with adjusted coordinates

            if not finishedLoadImages then
                return
            end

            if clickAudioControls() then
                return
            end

            -- react to exit dialog buttons
            if exitDialogActive and inBox(x, y, exitButtonPosX, exitButtonPosY, exitButtonWidth, exitButtonHeight) then
                love.window.setFullscreen(false)
                love.event.quit()
            end
            if exitDialogActive and inBox(x, y, noexitButtonPosX, noexitButtonPosY, noexitButtonWidth, noexitButtonHeight) then
                hideExitDialog()
                return
                --print("exit dialog closing")
            end

            -- on the titlescreen, start the game:
            if gameState == "title" then
                gameState = "playing"
                nextCustomerTime = love.timer.getTime() + 1
        
            -- on the end screen, do end screen things:
            elseif gameState == "ending" then
                if not exitDialogActive then
                    showExitDialog()
                end

            -- on the playing screen, do game things:
            else
                local pickedUp = nil
                 -- if your hand is empty...
                if screen.cursor:childCount() == 0 then
                    if boxShown and onButton(x, y) then
                        buttonClicked()
                        return
                    elseif boxShown and not onButton(x, y) then
                        toggleOrderBox()
                    elseif onCustomer(x, y) then
                        toggleOrderBox()
                    end
                    
                    -- try to get an item from anywhere
                    screen:handleClick(x,y)
                else
                    -- try to put items anywhere
                    screen:handleClick(x,y)
                end
            end
        else
            tlfres.setMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT, mouseStartX, mouseStartY)
        end
    end
end

function love.wheelmoved(x, y) 
    if not finishedLoadImages then
        return
    end
    
    if gameState == "playing" then
        screen.cursor:rotate(y * 5)
    end
end

function love.mousemoved(x, y, dx, dy)
    if not finishedLoadImages then
        return
    end

    if mouse_button_pressed then
        mouse_moved_distance = mouse_moved_distance + math.sqrt(dx * dx + dy * dy)
        if gameState == "playing" then
            screen.cursor:rotate(dy)
        end
    end
end

function love.draw()
    love.graphics.setColor(1, 1, 1, 1)
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)
    
    if not finishedLoadImages then
        drawTitleScreen()

        tlfres.endRendering()

        return
    end

    
    if gameState == "playing" then

        love.graphics.draw(images.background_new, 0, 0, 0, 0.5, 0.5)

        --draw success meters for current order
        drawOrderMeters()

        -- draw the scene graph, which currently contains sculupture, shelf, bin, box and cursor
        screen:draw()

        -- draw things related to the order, if there is one
        drawOrderBox()
        drawCustomer()

    elseif gameState == "title" then
        drawTitleScreen()
    elseif gameState == "ending" then
        drawEndScreen()
    end

    -- draw exitDialog if necessary
    drawExitDialog()

    -- draw audio control buttons
    drawAudioControls()

    tlfres.endRendering()
end
